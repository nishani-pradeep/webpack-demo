const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./src/index.js",
    plugins:[new HtmlWebpackPlugin({
        template:'./src/template.html'
    })],
    module:{
        rules:[
            // {
            //     test:/\.css$/,
            //     use:["style-loader", "css-loader"]
            // },
            {
                test:/\.scss$/,
                use:[
                    "style-loader", //3. injects css into style tag 
                    "css-loader",  //2. turns css into common js
                    'sass-loader' //1.Turns scss into css
                ]
            },
            {
                test:/\.html$/,
                use:["html-loader"]
            },
            {
                test:/\.(svg|png|jpg|gif)$/,
                use:{
                    loader:"file-loader",
                    options:{
                        name:"[name].[hash].[ext]",
                        publicPath: './',
                        outputPath:'imgs'
                    }
                }
            }
        ]
    }
}